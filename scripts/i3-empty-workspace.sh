#!/bin/sh
i3-msg workspace $(($(i3-msg -t get_workspaces | jq ' [ .[] | .num ] | max ') + 1))
