#!/bin/bash
workspace=$(($(i3-msg -t get_workspaces | jq ' [ .[] | .num ] | max ') + 1))
i3-msg move container to workspace $workspace

if  [[ $1 = "switch" ]]; then
	i3-msg workspace $workspace
fi

